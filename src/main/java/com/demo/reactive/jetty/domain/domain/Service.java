package com.demo.reactive.jetty.domain.domain;


public class Service {

    private String name;

    public Service() {
    }

    public Service(String name) {
        this.name = name + " " + Math.random();
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
